import 'package:dio/dio.dart';
import 'package:shop_app/config/config_api.dart';
import 'package:shop_app/model/pokemon_detail_result.dart';
import 'package:shop_app/model/pokemon_list_result.dart';

class RepoPokemon {
  Future<PokemonListResult> getPokemons() async { //Future 미래에 PokemonListResult가 올거야 (미래 예고)
    Dio dio = Dio();

    String _baseUrl = '$apiUri/fokemon/all';

    final response = await dio.get(_baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200; //200번 코드 = 성공
            }));
    return PokemonListResult.fromJson(response.data);
  }

  Future<PokemonDetailResult> getPokemon(num id) async { //Future 미래에 PokemonListResult가 올거야 (미래 예고)
    Dio dio = Dio();

    String _baseUrl = '$apiUri/fokemon/detail/{id}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200; //200번 코드 = 성공
            }));

    return PokemonDetailResult.fromJson(response.data);
  }
}