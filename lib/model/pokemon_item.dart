class PokemonItem {
  num id;
  String imgUrl;
  String name;
  num power;
  num physical;
  num defensive;
  num speed;

  PokemonItem(this.id, this.imgUrl, this.name, this.power, this.physical,
      this.defensive, this.speed);

  factory PokemonItem.fromJson(Map<String, dynamic> json) {
    return PokemonItem(
      json['id'],
      json['imgUrl'],
      json['name'],
      json['power'],
      json['physical'],
      json['defensive'],
      json['speed'],
    );
  }
}