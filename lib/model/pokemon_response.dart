class PokemonResponse {
  num id;
  String name;
  String fokemonType;
  num power;
  num physical;
  num defensive;
  num speed;
  String classify;
  String property;
  String etcMemo;
  String imgUrl;

  PokemonResponse(this.id, this.name, this.fokemonType,
  this.power, this.physical, this.defensive, this.speed, this.classify,
  this.property, this.etcMemo, this.imgUrl);

  factory PokemonResponse.fromJson(Map<String, dynamic> json) {
    return PokemonResponse(
        json['id'],
        json['name'],
        json['fokemonType'],
        json['power'],
        json['physical'],
        json['defensive'],
        json['speed'],
        json['classify'],
        json['property'],
        json['etcMemo'],
        json['imgUrl']
    );
  }
}