class BattleItem {
  num id;
  String imgUrl;
  String name;
  num power;
  num physical;
  num defensive;
  num speed;

  BattleItem(this.id, this.imgUrl, this.name, this.power, this.physical, this.defensive, this.speed);

  factory BattleItem.fromJson(Map<String, dynamic> json) {
    return BattleItem(
        json['id'],
        json['imgUrl'],
        json['name'],
        json['power'],
        json['physical'],
        json['defensive'],
        json['speed'],
    );
  }
}