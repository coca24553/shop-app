import 'package:flutter/material.dart';
import 'package:shop_app/component/component_battle.dart';
import 'package:shop_app/model/battle_item.dart';
import 'package:shop_app/model/pokemon_item.dart';
import 'package:shop_app/repository/repo_pokemon.dart';

class PageBattle extends StatefulWidget {
  const PageBattle({
    super.key,
    required this.id
  });
  final num id;

  @override
  State<PageBattle> createState() => _PageBattleState();
}

class _PageBattleState extends State<PageBattle> {
  BattleItem pokemon1 = BattleItem(1, 'assets/images/1_Pikachu.png', '피카츄', 20000, 10000, 5000, 6000);
  BattleItem pokemon2 = BattleItem(3, 'assets/images/3_Dragonite.png', '망나뇽', 160000, 80000, 8000, 2000);

  bool _isTurnPokemon1 = true; // 기본으로 1번 몬스터 선빵

  bool _pokemon1Live = true;
  num _pokemon1CurrentHp = 0;

  bool _pokemon2Live = true;
  num _pokemon2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 몬스터 선빵 판별
    setState(() {
      _pokemon1CurrentHp = pokemon1.physical; // 1번 몬스터 잔여 Hp (시작은 최대체력)
      _pokemon2CurrentHp = pokemon2.physical; // 2번 몬스터 잔여 Hp (시작은 최대체력)
    });
  }

  void _calculateFirstTurn() { //선빵 계산
    if (pokemon1.speed < pokemon2.speed) { // 몬스터 1번보다 몬스터 2번의 스피드가 높으면
      setState(() {
        _isTurnPokemon1 = false; // 2번 몬스터 선빵
      });
    }
  }

  num _calculateResultHitPoint(num myHitPower, num targetDefPower) { // 때렸을 때 공격력 몇? (크리티컬 계산식)
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10% 확률로 크리티컬
    criticalArr.shuffle(); // 셔플

    bool isCritical = false; // 기본 = 크리티컬 안 터짐
    if (criticalArr[0] == 1) isCritical = true; // 다 섞은 criticalArr에 0번째 요소가 1이면 크리티컬

    num resultHit = myHitPower; // 공격력
    if (isCritical) resultHit = resultHit * 2; // 크리티컬 뜨면 공격력 두배 효과
    resultHit = resultHit - targetDefPower; // 상대방 방어력만큼 데미지 까임
    resultHit = resultHit.round(); // 반올림

    if (resultHit <= 0) resultHit = 1; // 계산 결과 공격이 0이거나 음수일 때 공격포인트 고정 1 처리

    return resultHit;
  }

  void _checkIsDead(num targetPokemon) { // 상대 Hp 0 확인
    if (targetPokemon == 1 && (_pokemon1CurrentHp <= 0)) {
      setState(() {
        _pokemon1Live = false;
      });
    } else if (targetPokemon == 2 && (_pokemon2CurrentHp <= 0)) {
      setState(() {
        _pokemon2Live = false;
      });
    }
  }

  void _attPokemon(num actionPokemon) { // 공격 처리
    num myHisPower = pokemon1.power;
    num targetDefPower = pokemon2.defensive;

    if (actionPokemon == 2) {
      myHisPower = pokemon2.power;
      targetDefPower = pokemon1.defensive;
    }

    num resultHit = _calculateResultHitPoint(myHisPower, targetDefPower); // 상대 Hp 몇 까이는지 계산

    if (actionPokemon == 1) { // 공격하는 몬스터가 1번일 때
      setState(() {
        _pokemon2CurrentHp -= resultHit; // 2번 몬스터 체력 깍기
        if (_pokemon2CurrentHp <= 0) _pokemon2CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(2); // 2번 죽었는지 확인
      });
    } else {
      setState(() {
        _pokemon1CurrentHp -= resultHit; // 1번 몬스터 체력 깍기
        if (_pokemon1CurrentHp <= 0) _pokemon1CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(1); // 1번 죽었는지 확인
      });
    }

    setState(() {
      _isTurnPokemon1 = !_isTurnPokemon1; // 턴 넘기기. not 연산자로 계속 바꿔주기
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          foregroundColor: Colors.white,
          title:
          Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          )
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {

    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: 880,
            decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/images/main.jpeg'),
                )
            ),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width/2,
                      ),
                      ComponentBattle(
                        battleItem: pokemon1,
                        callback: () {
                          _attPokemon(1);
                        },
                        isMyTurn: _isTurnPokemon1,
                        isLive: _pokemon1Live,
                        hp: _pokemon1CurrentHp,
                      ),
                    ],
                  )
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: Text(
                    'VS',
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                      fontSize: 40,
                      color: Colors.red[800],
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      ComponentBattle(
                        battleItem: pokemon2,
                        callback: () {
                          _attPokemon(2);
                        },
                        isMyTurn: !_isTurnPokemon1,
                        isLive: _pokemon2Live,
                        hp: _pokemon2CurrentHp,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/2,
                      ),
                    ],
                  )
                ),
              ],
            ),
          ),
          Text(_gameLog),
        ],
      ),
    );
  }
}
