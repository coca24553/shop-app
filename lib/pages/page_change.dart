import 'package:flutter/material.dart';
import 'package:shop_app/pages/page_battle.dart';

class PageChange extends StatefulWidget {
  const PageChange({
    super.key
  });

  @override
  State<PageChange> createState() => _PageChangeState();
}

class _PageChangeState extends State<PageChange> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          foregroundColor: Colors.white,
          title:
          Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          )
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 880,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/main.jpeg'),
                  )
              ),
              child: Column(
                children: [
                  Container(
                      child: Row(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width/2,
                          ),
                          Container(
                            child: Column(
                              children: [
                                Image.asset('assets/images/1_Pikachu.png')
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            width: 100,
                            height: 40,
                            child: ElevatedButton(
                              style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.blue[400],
                                foregroundColor: Colors.white,
                              ),
                              onPressed: () {},
                              child: const Text(
                                '교체하기',
                                style: TextStyle(
                                    fontFamily: 'font_03'
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Text(
                      'VS',
                      style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: 40,
                        color: Colors.red[800],
                      ),
                    ),
                  ),
                  Container(
                      child: Row(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Image.asset('assets/images/3_Dragonite.png')
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width/2,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            width: 100,
                            height: 40,
                            child: ElevatedButton(
                              style: OutlinedButton.styleFrom(
                                backgroundColor: Colors.blue[400],
                                foregroundColor: Colors.white,
                              ),
                              onPressed: () {},
                              child: const Text(
                                '교체하기',
                                style: TextStyle(
                                    fontFamily: 'font_03'
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
