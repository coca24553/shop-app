import 'package:flutter/material.dart';
import 'package:shop_app/model/pokemon_response.dart';
import 'package:shop_app/repository/repo_pokemon.dart';
import 'package:shop_app/pages/page_battle.dart';

class PageDetail extends StatefulWidget {
  const PageDetail({
    super.key,
    required this.id
  });

  final num id;

  @override
  State<PageDetail> createState() => _PageDetailState();
}

class _PageDetailState extends State<PageDetail> {
  PokemonResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoPokemon().getPokemon(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          foregroundColor: Colors.white,
          title:
          Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          )
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_detail == null) { // 스켈레톤 ui
      return Text('로딩중');
    } else {
      return ListView(
        children: [
          Container(
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                          _detail!.imgUrl,
                        width: MediaQuery.of(context).size.width/2,
                      ),
                      Text(
                        _detail!.name,
                        style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: 20,
                          color: Colors.blue,
                        ),
                      ),
                      Text(
                        _detail!.fokemonType,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '${_detail!.power}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '${_detail!.physical}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '${_detail!.defensive}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '${_detail!.speed}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        _detail!.classify,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        _detail!.property,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        _detail!.etcMemo,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  width: 100,
                  height: 40,
                  child: ElevatedButton(
                    style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.blue[400],
                        foregroundColor: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBattle(id: widget.id)));
                    },
                    child: const Text(
                      '시작하기',
                      style: TextStyle(
                          fontFamily: 'font_03'
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      );
    }
  }
}
