import 'package:flutter/material.dart';
import 'package:shop_app/component/component_pokemon_item.dart';
import 'package:shop_app/model/pokemon_item.dart';
import 'package:shop_app/pages/page_battle.dart';
import 'package:shop_app/pages/page_detail.dart';
import 'package:shop_app/repository/repo_pokemon.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}
SliverGridDelegate _sliverGridDelegate() {
  return const SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2,
    mainAxisExtent: 350,
    mainAxisSpacing: 1,
  );
}

class _PageIndexState extends State<PageIndex> {
  List<PokemonItem> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에
    // setstate해서 _list 교체 할거임
    await RepoPokemon().getPokemons()
        .then((res) => {
          print(res),
      setState(() {
        _list = res.list;
      })
    })
        .catchError((err) => {
          debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        title:
          Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          )
      ),

      body: GridView.builder(
        itemCount: _list.length,
        gridDelegate: _sliverGridDelegate(),
        itemBuilder: (BuildContext context, int idx){
          return ComponentPokemonItem(
            pokemonItem: _list[idx],
            callback: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageDetail(id: _list[idx].id)));
            },
          );
        },
      ),
    );
  }
}
