import 'package:flutter/material.dart';
import 'package:shop_app/model/pokemon_item.dart';

class ComponentPokemonItem extends StatelessWidget {
  const ComponentPokemonItem({
    super.key,
    required this.pokemonItem,
    required this.callback

  });
  final PokemonItem pokemonItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Container(
              margin: EdgeInsets.all(20),
              width: 220,
              height: 310,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12.withOpacity(0.06),
                      spreadRadius: 10,
                      blurRadius: 30,
                      offset: Offset(0,0)
                  )
                ],
              ),
              child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(5),
                      child: Image.asset(
                        pokemonItem.imgUrl,
                        width: 180,
                        height: 180,
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Text(
                            pokemonItem.name,
                            style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: 18,
                              color: Colors.blue,
                            ),
                          ),
                          Text(
                            '공격력 ${pokemonItem.power}',
                            style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Light',
                              fontSize: 14,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                              '체력 ${pokemonItem.physical}',
                            style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Light',
                            fontSize: 14,
                            color: Colors.black,
                          ),
                          ),
                          Text(
                              '방어력 ${pokemonItem.defensive}',
                            style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Light',
                              fontSize: 14,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                              '속도 ${pokemonItem.speed}',
                            style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Light',
                              fontSize: 14,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    )
                  ]
              )
          ),
        ],
      )
    );

  }
}
