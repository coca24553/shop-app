import 'package:flutter/material.dart';
import 'package:shop_app/model/battle_item.dart';

class ComponentBattle extends StatefulWidget {

  const ComponentBattle({
    super.key,
    required this.battleItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.hp
  });

  final BattleItem battleItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num hp;

  @override
  State<ComponentBattle> createState() => _ComponentBattleState();
}

class _ComponentBattleState extends State<ComponentBattle> {
  double _calculateHpPercent() {
    return widget.hp / widget.battleItem.physical;
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width/3,
                  height: MediaQuery.of(context).size.width/3,
                  child: Image.asset(
                    '${widget.isLive ? widget.battleItem.imgUrl : 'assets/images/sad.jpg'}',
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text(
                        widget.battleItem.name,
                        style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: 20,
                          color: Colors.blue,
                        ),
                      ),
                      Text(
                        '공격력 ${widget.battleItem.power}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '총 HP ${widget.battleItem.physical}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '방어력 ${widget.battleItem.defensive}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        '속도 ${widget.battleItem.speed}',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Column(
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: LinearProgressIndicator(
                              value: _calculateHpPercent(),
                              backgroundColor: Colors.grey[300],
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.red
                              ),
                              minHeight: 15.0,
                              semanticsLabel: 'semanticsLabel',
                              semanticsValue: 'semanticsValue',
                            ),
                          )
                      ),
                    ],
                  ),
                ),
                Container(
                  child: (widget.isMyTurn && widget.isLive) ?
                  ElevatedButton(
                      style: OutlinedButton.styleFrom(
                          backgroundColor: Colors.blue,
                          foregroundColor: Colors.white,
                      ),
                      onPressed: widget.callback,
                      child: Text(
                        '공격',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold'
                        ),
                      )
                  ) : Container(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
